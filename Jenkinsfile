#!/usr/bin/env groovy

def version = VersionNumber(versionNumberString: '${BUILD_DATE_FORMATTED,"yyyyMMdd"}.${BUILDS_TODAY_Z}')
def storage = "image-storage"
def image = "debian-${version}.img"
def image_url = "https://images.collabora.co.uk/${image}"

def docker_registry_name = 'docker-registry.collabora.com'
def debos_image_name = 'debos:latest'
def convert_image_name = 'mgalka/convert-results:latest'


node {
  stage ('Checkout SCM') {
    checkout scm
  }

    debosImage = docker.image("${docker_registry_name}/${debos_image_name}")
    stage ('Build image') {
        debosImage.inside("--device /dev/kvm --security-opt label:disable"){
          dir(version){
            sh "debos -t image:${image} -t basename:${version} ${workspace}/debos/uefi/debos-qemu-amd64.yaml"
          }
        }
      }
      stage ('Push image') {
          debosImage.inside("--device /dev/kvm --security-opt label:disable --env NSS_WRAPPER_PASSWD='/tmp/passwd' --env NSS_WRAPPER_GROUP='/dev/null'"){
            if (env.gitlabBranch == null) {
              sshagent (credentials: [ "images.ccu-upload", ] ) {
                sh 'echo docker:x:$(id -u):$(id -g):docker gecos:/tmp:/bin/false > /tmp/passwd'
                sh "LD_PRELOAD=libnss_wrapper.so scp -oStrictHostKeyChecking=no -r ${version} jenkins-upload@images.collabora.co.uk:/srv/jenkins-upload/image-uploads/elce-demo/"
              }
            }
          }
      }
}

def hook
node {
  stage ('Submit Test Job') {
    hook = registerWebhook()
    debosImage.inside("--device /dev/kvm --security-opt label:disable"){
      withCredentials([file(credentialsId: 'LQA_CONFIG_FILE', variable: 'LQARC')]) {
        sh (script: "lqa -c ${LQARC} submit -t callback_url:${hook.getURL()} -t image:${image_url} lava-qemu-amd64.yaml")
      }
    }
  }
}

def result
stage("Wait for LAVA") {
  result = waitForWebhook hook
}

stage("Parsing results") {
  node {
    checkout scm
    convertImage = docker.image("${docker_registry_name}/${convert_image_name}")
    convertImage.inside("--device /dev/kvm --security-opt label:disable") {
          writeFile(file: "results.json", text: result)
          sh(script: "convert-results results.json results.xml")
    }
    junit("results.xml")
  }
}
