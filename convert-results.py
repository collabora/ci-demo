import yaml
import json
import sys
from junit_xml import TestSuite, TestCase

LAVA_KEY = 'lava'

print(sys.argv[1])

j = json.load(open(sys.argv[1]))
suites = []

for suite,results in j['results'].items():
    cases = []
    results_yaml = yaml.load(results)
    for case_data in results_yaml:
        duration = case_data['metadata'].get('duration', 0)
        print(duration)
        case = TestCase(case_data['name'])
        if case_data['result'] != "pass":
            case.add_failure_info(message = case_data.get('url'))
        cases.append(case)
    ts = TestSuite(suite, cases)
    suites.append(ts)

open(sys.argv[2], "w").write(TestSuite.to_xml_string(suites))